'use strict';

/**
 * @ngdoc overview
 * @name dataTablarApp
 * @description
 * # dataTablarApp
 *
 * Main module of the application.
 */
angular
    .module('dataTablarApp', [
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'dataTablar',
        'angular-jwt'
    ])
    .constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        refreshSuccess: 'auth-refresh-token-success',
        refreshFailed: 'auth-refresh-token-failed',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
    })
    .constant('USER_ROLES', {
        admin: 'admin',
        user: 'user',
        guest: 'guest'
    })
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/data-tablar.html',
                controller: 'MainCtrl',
            })
            .otherwise({
                redirectTo: '/'
            });
    }])
    .config(['$httpProvider', 'jwtInterceptorProvider', function Config($httpProvider, jwtInterceptorProvider) {
        jwtInterceptorProvider.tokenGetter = [
            'config',
            'jwtHelper',
            'authService',
            '$cookies',
            'Notification',
            function(config, jwtHelper, authService, $cookies, Notification) {
                if (config.url.indexOf('data-tablar-server.herokuapp.com') > -1) {
                    return null;
                }
                if (config.url.substr(config.url.length - 5) == '.html') {
                    return null;
                }

                var currentRequestToken = $cookies.get('request_token');
                var currentRefreshToken = $cookies.get('refresh_token');

                //if request_token and refresh_token are already defined in cookies. Then we check the expiration
                if (currentRefreshToken && currentRequestToken) {
                    if (!jwtHelper.isTokenExpired(currentRefreshToken)) {
                        if (jwtHelper.isTokenExpired(currentRequestToken)) {
                            var data = {
                                refresh_token: currentRefreshToken,
                                request_token: currentRequestToken
                            };
                            return authService
                                .refreshToken(data)
                                .then(function(result) {
                                    $cookies.put('request_token', result.request_token);
                                    return result.request_token;
                                }, function(result) {
                                    Notification.error('You will be logged out because of failing to refresh the request token. ');
                                    authService.logout();
                                });
                        } else {
                            return currentRequestToken;
                        }
                    } else {
                        Notification.error('You will logged out because of the expired refresh token. ');
                        authService.logout();
                    }
                } else {
                    if (currentRefreshToken || currentRequestToken) {
                        authService.logout();
                    }
                }
            }
        ];

        $httpProvider.interceptors.push('jwtInterceptor');
    }])
    .run([
        '$rootScope',
        'authService',
        'Notification',
        'AUTH_EVENTS',
        '$cookies',
        'jwtHelper',
        '$route',
        function($rootScope, authService, Notification, AUTH_EVENTS, $cookies, jwtHelper, $route) {
            $rootScope.currentUser = $cookies.getObject('currentUser');

            $rootScope.credentials = {
                email: '',
                password: ''
            };
            $rootScope.logout = function() {
                authService.logout();
                $route.reload();
            };

            $rootScope.refreshToken = function(currentRefreshToken) {
                var data = {
                    refresh_token: currentRefreshToken
                };
                authService
                    .refreshToken(data)
                    .then(function(result) {
                        $cookies.put('request_token', result.request_token);
                        $rootScope.$broadcast(AUTH_EVENTS.refreshSuccess);
                    }, function(result) {
                        Notification.error('Refresh token failed. Logout now!');
                        $rootScope.$broadcast(AUTH_EVENTS.refreshFailed);

                        //If failed, logout
                        $rootScope.logout();
                    });
            };
            $rootScope.login = function(credentials) {
                authService
                    .login(credentials)
                    .then(function(result) {
                        if (result.request_token && result.refresh_token) {
                            try {
                                //decode request_token payload
                                var requestTokenPayload = jwtHelper.decodeToken(result.request_token);

                                //Set currentUser
                                var currentUser = {
                                    name: requestTokenPayload.name,
                                    role: requestTokenPayload.role
                                };

                                //store cookies
                                $cookies.putObject('currentUser', currentUser);
                                $cookies.put('request_token', result.request_token);
                                $cookies.put('refresh_token', result.refresh_token);

                                //Global currentUser
                                $rootScope.currentUser = $cookies.getObject('currentUser');

                                //broadcast event
                                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);

                                //clear form credentials.
                                $rootScope.credentials = {
                                    email: '',
                                    password: ''
                                };

                                //Notify message
                                Notification.success('Login succeed');

                                $route.reload();
                            } catch (e) {
                                $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                                Notification.error('Login failed');
                            }
                        } else {
                            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                            Notification.error('Login failed');
                        }
                    }, function(result) {
                        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                        Notification.error('Login failed');
                    });
            };
            console.log(authService);
        }
    ]);