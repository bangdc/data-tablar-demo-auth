'use strict';
/**
 * @ngdoc service
 * @name dataTablarApp.service:authService
 * 
 * @requires $http
 * 
 * @description
 * # authService
 * Provided services for communicating with remote server by HTTP requests.
 */
angular.module('dataTablarApp')
    .factory('authService', [
        '$http',
        '$q',
        '$cookies',
        '$rootScope',
        'AUTH_EVENTS',
        function($http, $q, $cookies, $rootScope, AUTH_EVENTS) {
            return {
                login: login,
                refreshToken: refreshToken,
                logout: logout
            };

            /**
             * @ngdoc method
             * @name dataTablarApp.service: authService # login
             * @methodOf dataTablarApp.service:authService
             * 
             * @description
             * 
             * @param {object} credentials email and password object.
             **/
            function login(credentials) {
                return $http
                    .post('http://localhost:5001/login', credentials)
                    .then(function(response) {
                        if (response.status === 200) {
                            return response.data;
                        } else {
                            return $q.reject(response);
                        }
                    }, function(response) {
                        return $q.reject(response);
                    });
            }

            /**
             * @ngdoc method
             * @name dataTablarApp.service: authService # logout
             * @methodOf dataTablarApp.service:authService
             * 
             * @description
             **/
            function logout() {
                //remove cookies
                $cookies.remove('currentUser');
                $cookies.remove('request_token');
                $cookies.remove('refresh_token');
                //remove global currentUser
                delete $rootScope.currentUser;
                //broadcast event.
                $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            }

            /**
             * @ngdoc method
             * @name dataTablarApp.service: authService # refreshToken
             * @methodOf dataTablarApp.service:authService
             * 
             * @description
             * 
             * @param {object} data one property currentRefreshToken
             **/
            function refreshToken(data) {
                return $http({
                        url: 'http://localhost:5001/refreshToken',
                        // This makes it so that this request doesn't send the JWT
                        // Without it, the application will be crashed because of looping over again.
                        skipAuthorization: true,
                        method: 'POST',
                        data: data
                    })
                    .then(function(response) {
                        if (response.status === 200) {
                            return response.data;
                        } else {
                            return $q.reject(response);
                        }
                    }, function(response) {
                        return $q.reject(response);
                    });
            }
        }
    ]);